#r "System.Xml.Linq"

open System
open System.Xml.Linq
open System.Xml.XPath

let xml = XDocument.Parse(Console.In.ReadToEnd())
let includes = xml.XPathEvaluate("//*[local-name()='Compile']/@Include") :?> Collections.IEnumerable |> Seq.cast<XAttribute>
String.concat " " [for x in includes -> x.Value] |> printfn "%O"
