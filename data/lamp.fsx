
#I "bin"
#r "bin/dawnlib.dll"
#r "bin/OpenTK.dll"

open System
open DawnLib
open System.IO
open OpenTK

let mesh = DawnLib.Loaders.loadOBJ <| File.OpenRead "lamp.obj"
mesh.CalcNormals()
let renderer = DawnLib.Renderer.GL3.Renderer()
let win = renderer.app [ mesh ]
let mutable time = 0.0f
let up (e:OpenTK.FrameEventArgs) =
    time <- time + float32 e.Time
    let position = Vector3(10.0f * sin (time), 20.f, 10.0f * cos time)
    let m = Matrix4.LookAt(position, Vector3.Zero, Vector3.UnitY) in renderer.modelview <- m
    let m = Matrix4.CreatePerspectiveFieldOfView(1.0f, 1.0f, 0.1f, 100.0f) in renderer.proj <- m
    ()
win.UpdateFrame.Add up
win.Run(10.0, 10.0)
// DawnLib.App.render_meshes [ mesh ] 0
