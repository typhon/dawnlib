#I "../bin"
#r "dawnlib.dll"
#r "OpenTK.dll"

open System
open System.IO
open OpenTK

let window = new DawnLib.Apps.RenderApp.RenderStandaloneWindow()
Async.Start(async {
    File.OpenRead "data/lamp.obj" |> DawnLib.Loaders.OBJFile.loadOBJ |> window.Meshes.Add
    File.OpenRead("data/level.vox")|> DawnLib.Loaders.VOXFile.read |> window.Voxels.Add
    File.OpenRead("data/monu9.vox")|> DawnLib.Loaders.VOXFile.read |> window.Voxels.Add
    File.OpenRead("data/castle.vox")|> DawnLib.Loaders.VOXFile.read |> window.Voxels.Add
})
window.Run()
