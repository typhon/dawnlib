#version 330

layout (points) in;
layout (points) out;
layout (max_vertices = 1) out;

out vec3 normal;

void main() {
    if (gl_in.length() > 2)
        normal = normalize(cross(gl_in[1].gl_Position - gl_in[0].gl_Position, gl_in[2].gl_Position - gl_in[0].gl_Position));
    else
        normal = vec3(0, 0, 1);
}
