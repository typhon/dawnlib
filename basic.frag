#version 330

in vec3 _nor;
in vec3 _pos;
in vec3 _bary;
in vec4 _col;
out vec4 out_color;

vec3 sun = normalize(vec3(0.2, 2.0, 0.8));

vec4 phong(vec4 color, vec3 normal) {
    sun = normalize(sun);
    normal = normalize(normal);
    float x = max(dot(normal, sun), 0);
    return color;
    // return vec4(normal * 0.5 + 0.5, 1);
}

void main() {
    out_color = phong(_col, _nor);
}
