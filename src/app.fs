module DawnLib.App
#nowarn "58"

open DawnLib.Base
open DawnLib.Core
open DawnLib.Renderers
open OpenTK
open OpenTK.Input
open OpenTK.Graphics
open OpenTK.Graphics.OpenGL
open System.Drawing
open System.Drawing.Imaging


let rec screenshot (this:GameWindow) =
    let bmp = new Bitmap(this.ClientSize.Width, this.ClientSize.Height)
    let data = bmp.LockBits(this.ClientRectangle, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb)
    GL.ReadPixels(0, 0, this.ClientSize.Width, this.ClientSize.Height, PixelFormat.Bgr, PixelType.UnsignedByte, data.Scan0)
    bmp.UnlockBits(data)
    bmp.RotateFlip(RotateFlipType.RotateNoneFlipY)
    bmp

let ScreenShotBehavior (window:GameWindow) =
    window.KeyDown.Add (fun e -> match e.Key with
                        | Key.PrintScreen -> let b = screenshot window in b.Save("screen.png")
                        | _ -> ())

let QuitKeyBehavior (window:GameWindow) =
    window.KeyDown.Add (fun e -> match e.Key with
                        | Input.Key.Q | Input.Key.Escape -> window.Exit()
                        | _ -> ())

let MeshDebugStats (window: GameWindow) (meshes: Mesh IGameSystem) =
    let action e =
        printfn "Mesh Count: %d" (Seq.length meshes.Items)
        printfn "Face Count: %d" (meshes.Items |> Seq.sumBy (fun m -> m.indices.Count / 3))
        printfn "FPS: %f" (1.0 / window.RenderTime)
    window.KeyDown |> Event.filter (fun e -> e.Key = Key.Number1) |> Event.add action

let ToggleRenderOptions (window: GameWindow) (renderer: GL3.Renderer) =
    window.KeyDown.Add (
        fun e ->
        match e.Key with
        | Input.Key.Keypad0
        | Input.Key.Keypad1
        | Input.Key.Keypad2
        | Input.Key.Keypad3
        | Input.Key.Keypad4
        | Input.Key.Keypad5
        | Input.Key.Keypad6
        | Input.Key.Keypad7
        | Input.Key.Keypad8
        | Input.Key.Keypad9 ->
            let i = float32 (e.Key - Input.Key.Keypad0)
            renderer.data <- Vector4(i, i, i, i)
        | _ -> ())

let TrackBallCameraBehavior (window:GameWindow) (renderer: GL3.Renderer) =
    let mutable distance = 10.0f
    let mutable height = 2.0f
    let mutable fov = 0.7f
    let mutable aspect = 1.0f
    let mutable angle = 0.0f
    let mutable target = V3.Zero
    let onrender e =
        let projection = Matrix4.CreatePerspectiveFieldOfView(fov, aspect, 0.1f, 100.0f)
        let modelView = Matrix4.LookAt(
            target + V3(distance * sin angle, height, distance * cos angle),
            target,
            V3.UnitY)
        renderer.modelview <- modelView
        renderer.proj <- projection
    window.RenderFrame.Add onrender
    let onupdate e =
        if window.Keyboard.[Key.ShiftLeft] || window.Keyboard.[Key.ShiftRight] then
            if window.Keyboard.[Key.A] then target <- target + Vector3.UnitX * 0.1f
            if window.Keyboard.[Key.D] then target <- target - Vector3.UnitX * 0.1f
            if window.Keyboard.[Key.W] then target <- target + Vector3.UnitZ * 0.1f
            if window.Keyboard.[Key.S] then target <- target - Vector3.UnitZ * 0.1f
            if window.Keyboard.[Key.Space] then target <- V3.Zero
        else
            if window.Keyboard.[Key.A] then angle <- angle + 0.1f
            if window.Keyboard.[Key.D] then angle <- angle - 0.1f
            if window.Keyboard.[Key.W] then distance <- distance * 0.95f
            if window.Keyboard.[Key.S] then distance <- distance / 0.95f
    window.UpdateFrame.Add onupdate

let save_mesh (mesh:Mesh) (name:string) =
    let writer = new System.IO.StreamWriter(name)
    for v in mesh.vertices do writer.WriteLine("v {0} {1} {2}", v.pos.X, v.pos.Y, v.pos.Z);
    let rec loop ii =
        match List.ofSeq ii with
        | i::j::k::xs -> writer.WriteLine("f {0} {1} {2}", i+1, j+1, k+1); loop xs
        | _ -> ()
    loop (mesh.indices |> Seq.toList)
    writer.Close()
