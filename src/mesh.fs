module DawnLib.Mesh

open OpenTK
open OpenTK.Graphics
open System.Collections
open System.Linq
open System.Runtime.InteropServices


[<StructLayout(LayoutKind.Sequential, Pack=1)>]
type Vertex = struct
    val mutable col: Color4
    val mutable pos: Vector3
    val mutable nor: Vector3
    val mutable uv: Vector2
    val mutable bary: Vector3
end

type Mesh () =
    member val vertices = new ResizeArray<Vertex>()
    member val indices = new ResizeArray<int>()
    member this.CalcNormals() =
        for triplet in Seq.chunkBySize 3 (this.indices) do
          match triplet with
          | [|i; j; k|] ->
            let mutable a = this.vertices.[i]
            let mutable b = this.vertices.[j]
            let mutable c = this.vertices.[k]
            let nor = Vector3.Cross(b.pos - a.pos, c.pos - a.pos).Normalized()
            a.nor <- nor
            b.nor <- nor
            c.nor <- nor
            this.vertices.[i] <- a
            this.vertices.[j] <- b
            this.vertices.[k] <- c
            | _ -> ()

    static member fromTriangles t =
        let m = new Mesh()
        m.vertices.AddRange t
        m.indices.AddRange(Enumerable.Range(0, Seq.length t))
        m

type Box (min:Vector3, max:Vector3, mesh:Mesh) =
    member val Min = min
    member val Max = max
    static member OfMesh (m:Mesh) =
        let min = m.vertices |> Seq.map (fun v -> v.pos) |> Seq.reduce (fun a b -> Vector3.ComponentMin(a,b))
        let max = m.vertices |> Seq.map (fun v -> v.pos) |> Seq.reduce (fun a b -> Vector3.ComponentMax(a,b))
        Box(min, max, m)

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module Box =
    let intersect (a:Box) (b:Box) =
        (a.Min.X < b.Max.X && b.Min.X < a.Max.X) &&
        (a.Min.Y < b.Max.Y && b.Min.Y < a.Max.Y) &&
        (a.Min.Z < b.Max.Z && b.Min.Z < a.Max.Z)
