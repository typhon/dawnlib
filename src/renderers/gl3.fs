namespace DawnLib.Renderers

open OpenTK
open OpenTK.Graphics.OpenGL;

#nowarn "58"

open System
open System.Collections.Generic
open OpenTK
open OpenTK.Graphics.OpenGL;
open DawnLib.Core
open DawnLib.Base
open DawnLib.Mesh

module GL3 =
    let xprintfn format arg = match arg with | "" -> () | arg -> Printf.printfn format arg

    type DepthBuffer (w, h) =
        do
            let depth = GL.GenTexture()
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, depth)
            GL.RenderbufferStorage(
                RenderbufferTarget.Renderbuffer,
                RenderbufferStorage.DepthComponent32, w, h)

    type VBO (mesh:Mesh) =
        let vao = GL.GenVertexArray()
        let buf = GL.GenBuffer()
        let ibuf = GL.GenBuffer()
        let count = mesh.indices.Count
        let bufferData (m:Mesh) =
            let data = m.vertices.ToArray()
            let idata = m.indices.ToArray()
            GL.BindBuffer(BufferTarget.ArrayBuffer, buf)
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * sizeof<Vertex>, data, BufferUsageHint.StaticDraw)
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ibuf)
            GL.BufferData(BufferTarget.ElementArrayBuffer, idata.Length * sizeof<int>, idata, BufferUsageHint.StaticDraw)
        let bind () =
            GL.BindBuffer(BufferTarget.ArrayBuffer, buf)
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ibuf)
        let setupArray (m:Mesh) =
            GL.BindVertexArray(vao)
            bind ()
            GL.EnableVertexAttribArray(0)
            GL.EnableVertexAttribArray(1)
            GL.EnableVertexAttribArray(2)
            GL.EnableVertexAttribArray(3)
            GL.EnableVertexAttribArray(4)
            let offsetof prop = System.Runtime.InteropServices.Marshal.OffsetOf<Vertex> prop
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, sizeof<Vertex>, offsetof "pos")
            GL.VertexAttribPointer(1, 4, VertexAttribPointerType.Float, false, sizeof<Vertex>, offsetof "col")
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, false, sizeof<Vertex>, offsetof "nor")
            GL.VertexAttribPointer(3, 2, VertexAttribPointerType.Float, false, sizeof<Vertex>, offsetof "uv")
            GL.VertexAttribPointer(4, 3, VertexAttribPointerType.Float, false, sizeof<Vertex>, offsetof "bary")
        do
            bufferData mesh
            setupArray mesh

        member this.Update m = bufferData m
        member this.Count = count
        member this.Bind () =
            GL.BindVertexArray(vao)
            bind ()
        interface IDisposable with member this.Dispose() = GL.DeleteBuffers(2, [|buf; ibuf|])

    type Shader (stype:ShaderType, src:string) =
        let id = GL.CreateShader stype
        do
            GL.ShaderSource(id, src)
            GL.CompileShader(id)
            GL.GetShaderInfoLog(id) |> xprintfn "compile: %s"
        member this.ID = id
        interface IDisposable with member this.Dispose() = GL.DeleteShader id

    type Program (shaders:seq<Shader>) =
        let id = GL.CreateProgram()
        do
            for s in shaders do GL.AttachShader(id, s.ID)
            GL.LinkProgram(id)
            GL.GetProgramInfoLog(id) |> xprintfn "link: %s"
            match GL.GetError () with
                | ErrorCode.NoError -> ()
                | e -> printfn "Error: %O" e
        member this.ID = id
        member this.Enable () = GL.UseProgram id
        interface IDisposable with member this.Dispose() =
            GL.DeleteProgram id
            for s in shaders do let d = (s :> IDisposable) in d.Dispose()

    type Renderer () as this =
        let memo = LazyDict(fun m -> new VBO(m))
        let mutable mv = Matrix4.Identity
        // let mutable p = Matrix4.CreatePerspectiveFieldOfView(0.7f, 1.0f, 0.01f, 10.0f);
        let mutable p = Matrix4.Identity
        let basicProgram () =
          let p = new Program([
            new Shader(ShaderType.VertexShader, IO.File.ReadAllText("basic.vert"))
            new Shader(ShaderType.FragmentShader, IO.File.ReadAllText("basic.frag")) ])
          match GL.GetError () with
            | ErrorCode.NoError -> ()
            | e -> failwith (Printf.sprintf "Program Error: %O" e)
          p
        let mutable lazyshader = lazy(basicProgram())
        let timer = new System.Diagnostics.Stopwatch()
        do
            timer.Start()
        let draw (vbo:VBO) =
            GL.Enable(EnableCap.DepthTest)
            let bb = lazyshader.Force()
            bb.Enable()
            vbo.Bind()
            GL.UniformMatrix4(GL.GetUniformLocation(bb.ID, "mv"), false, ref mv)
            GL.UniformMatrix4(GL.GetUniformLocation(bb.ID, "p"), false, ref p)
            GL.Uniform4(GL.GetUniformLocation(bb.ID, "data"), ref this.data)

            GL.DrawElements(PrimitiveType.Triangles, vbo.Count, DrawElementsType.UnsignedInt, 0)
            if timer.ElapsedMilliseconds > this.ReloadShaderTime then
                timer.Restart()
                (bb :> IDisposable).Dispose()
                lazyshader <- lazy(basicProgram())
        member val fbo = 0 with get, set
        member val ReloadShaderTime = 5000L with get, set
        member val data = Vector4.Zero with get, set
        member this.BeginRender () = ()
        member this.EndRender() = ()
        member this.Draw(mesh:Mesh) = draw (memo.[mesh])
        member this.Draw(vbo: VBO) = draw vbo
        member this.modelview with set v = mv <- v
        member this.proj with set v = p <- v
        member this.app (meshes:Mesh list) =
            let window = new OpenTK.GameWindow(
                600, 600, Graphics.GraphicsMode.Default, "title",
                GameWindowFlags.Default, DisplayDevice.Default, 3, 3,
                Graphics.GraphicsContextFlags.Default)
            let render e =
                if this.fbo = 0 then
                    this.fbo <- GL.GenFramebuffer()
                    let rbuf = GL.GenRenderbuffer()
                    GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, rbuf)
                    GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer,
                                           RenderbufferStorage.Rgba8,
                                           800, 800);
                    GL.BindFramebuffer(FramebufferTarget.Framebuffer, this.fbo)
                    GL.FramebufferRenderbuffer(
                        FramebufferTarget.Framebuffer,
                        FramebufferAttachment.ColorAttachment0,
                        RenderbufferTarget.Renderbuffer,
                        rbuf)

                // GL.BindFramebuffer(FramebufferTarget.Framebuffer, this.fbo)
                GL.ClearColor(Graphics.Color4.CornflowerBlue)
                GL.Clear(ClearBufferMask.ColorBufferBit ||| ClearBufferMask.DepthBufferBit)
                for m in meshes do this.Draw m
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0)
                // // GL.Begin(PrimitiveType.Triangles)
                GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, this.fbo)
                GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, 0)
                GL.BlitFramebuffer(
                    0, 0, 400, 400, 0, 0, 400, 400,
                    ClearBufferMask.ColorBufferBit,
                    BlitFramebufferFilter.Nearest);
                GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, 0)
                GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, 0)
                window.SwapBuffers()
            window.RenderFrame.Add render
            window
