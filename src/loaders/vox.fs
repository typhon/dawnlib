module DawnLib.Loaders.VOXFile
open DawnLib.Mesh
open System
open System.IO
open OpenTK

type System.IO.BinaryReader with
    member this.ReadNStr n = new string(this.ReadChars(n))

type Chunk = { id:string; data:byte[]; children:Chunk list }
with
    member this.size = 12 + this.data.Length + (this.children |> List.sumBy (fun x -> x.size))
    override this.ToString() =
        let rec fmt d (chunk:Chunk) =
            let s = sprintf "%s%s %d" (String.replicate d "  ") chunk.id chunk.data.Length
            s :: List.collect (fmt (d + 1)) chunk.children
        String.concat "\n" (fmt 0 this)

let rec private readChunk (r:BinaryReader) remaining =
    let id = r.ReadNStr 4
    let size = r.ReadInt32()
    let childsize = r.ReadInt32()
    let data = r.ReadBytes size
    let children = List.unfold (fun s -> if s <= 0 then None else let c:Chunk = readChunk r s in Some(c, s - c.size)) childsize
    { id=id; data=data; children=children }

let dimensionData chunk =
    let readInt o = BitConverter.ToInt32(chunk.data, o)
    readInt 0, readInt 4, readInt 8

let voxelData chunk =
    let rr = new BinaryReader(new MemoryStream(chunk.data))
    let num = rr.ReadInt32()
    [| for i in 1 .. num -> let a = rr.ReadBytes(4) in (int a.[0], int a.[2], int a.[1], a.[3]) |]

let paletteData chunk =
    let rr = new BinaryReader(new MemoryStream(chunk.data))
    Array.init 256 (fun i -> new Graphics.Color4(chunk.data.[4 * i], chunk.data.[4 * i+1], chunk.data.[4 * i+2], chunk.data.[4 * i+3]))

let read (stream:Stream) =
    let r = new BinaryReader(stream)
    r.ReadBytes 8 |> ignore
    let chunk = readChunk r Int32.MaxValue
    printfn "Chunk:\n%O" chunk
    let dim = let i, j, k = dimensionData chunk.children.[0] in i, k, j
    let blocks = [| for a,b,c,d in voxelData chunk.children.[1] -> a, b, c, DawnLib.Voxels.Block(value=d) |]

    printfn "dim: %A" dim
    printfn "max: %A" ((Array.max [| for a,b,c,d in blocks -> a |]),
                       (Array.max [| for a,b,c,d in blocks -> b |]),
                       (Array.max [| for a,b,c,d in blocks -> c |]))
    printfn "colors: %A" ( blocks |> Seq.groupBy (fun (a,b,c,d) -> d) |> Seq.map (fun (k, g) -> k.value, Seq.length g))
    let mutable v = DawnLib.Voxels.BlockChunk.FromSeq (new DawnLib.Core.Vector3i(dim) / -2) dim blocks
    v.palette <- if chunk.children.Length > 2 then paletteData chunk.children.[2] else Array.zeroCreate 256
    v
