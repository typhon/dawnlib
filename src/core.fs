// Not really *core* functionality, just useful utils that the rest of the core layer uses
module DawnLib.Core

open OpenTK
open OpenTK.Graphics
open System
open System.Collections.Generic

// utility methods for float32 randoms
type System.Random with
    member this.NextFloat() = this.NextDouble() |> float32
    member this.NextFloat(max:float32) = this.NextFloat() * max
    member this.NextV3() = Vector3(this.NextFloat(), this.NextFloat(), this.NextFloat())


let nextf (rand:Random) n = rand.NextFloat() * n

// random normalized vector
let rec nextv rand =
    let x = nextf rand 2.0f - 1.0f
    let y = nextf rand 2.0f - 1.0f
    let z = nextf rand 2.0f - 1.0f
    if (abs(x * y * z) < 0.01f) then nextv rand
    else Vector3(x,y,z).Normalized()
let __rand = new System.Random()
let randf = nextf __rand
let randv () = nextf __rand

// defaultdict container with initialization function -- good for memoizing
type LazyDict<'a, 'b when 'a : equality> (func) =
    let data = Dictionary<'a,'b>()
    member this.Item
        with get k =
          match data.TryGetValue k with
            | true, v -> v
            | false, _ -> let v = func k in data.[k] <- v; v

type Vector3i = struct
    val mutable X: int
    val mutable Y: int
    val mutable Z: int
    new(a,b,c) = { X=a; Y=b; Z=c }
    new((a,b,c)) = {X=a; Y=b; Z=c }
    static member ( + ) (v:Vector3i, u:Vector3i) = Vector3i(v.X + u.X, v.Y + u.Y, v.Z + u.Z)
    static member ( * ) (v:Vector3i, u:int) = Vector3i(v.X * u, v.Y * u, v.Z * u)
    static member ( / ) (v:Vector3i, u:int) = Vector3i(v.X / u, v.Y / u, v.Z / u)
    override this.ToString () = sprintf "(%d,%d,%d)" this.X this.Y this.Z
end

type Vector3 with
    member this.AngleTo(that:Vector3) =
        Vector3.Dot(this, that) / this.Length / that.Length |> acos

type OpenTK.Graphics.Color4 with
    member this.Mult(f:float32) = Color4(this.R * f, this.G * f, this.B * f, this.A)
    member this.Lerp(o:Color4, f:float32) =
        let g = 1.f - f
        new Color4(g * this.R + f * o.R, g * this.G + f *o.G,
                   g * this.B + f * o.B,
                   g * this.A + f * o.A)

type IGameSystem<'T> = interface
    abstract Items: 'T seq
    abstract Added: IEvent<'T>
    abstract Removed: IEvent<'T>
end

type BaseGameSystem<'T when 'T : equality> () =
    let items = new ResizeArray<'T>()
    let itemAdded = new Event<'T>()
    let itemRemoved = new Event<'T>()

    member this.Add x =
        itemAdded.Trigger x
        items.Add x

    member this.Remove x =
        itemRemoved.Trigger x
        items.Remove x

    member this.Map (func: 'T -> 'U) =
        let newSystem = new BaseGameSystem<'U>()
        Event.add (func >> newSystem.Add) itemAdded.Publish
        Event.add (func >> newSystem.Remove >> ignore) itemRemoved.Publish
        newSystem

    /// like map, but the mapper function is always run on the UI thread of the window
    member this.MapWithMainThreadPoll (window:GameWindow) (func: 'T -> 'U) =
        let newSystem = new BaseGameSystem<'U>()
        let dataMap = System.Collections.Generic.Dictionary<'T, 'U>()
        let queue = System.Collections.Concurrent.ConcurrentBag<'T * bool>()
        Event.add (fun t -> queue.Add (t, true)) itemAdded.Publish
        Event.add (fun t -> queue.Add (t, false)) itemRemoved.Publish
        let poll evt =
            match queue.TryTake() with
            | true, (item, true) ->
                dataMap.Add(item, func item)
                newSystem.Add(dataMap.[item])
            | true, (item, false) ->
                ignore(newSystem.Remove (dataMap.[item]))
                ignore(dataMap.Remove(item))
            | _ -> ()
        Event.add poll window.UpdateFrame
        newSystem

    member this.Items = items :> 'T seq
    member this.Added = itemAdded.Publish
    member this.Removed = itemRemoved.Publish

    interface IGameSystem<'T> with
        member this.Items = this.Items
        member this.Added = this.Added
        member this.Removed = this.Removed
